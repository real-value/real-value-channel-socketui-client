//let debug = require('debug')('ChannelSocketIOClient')

let debug = console.log

function ChannelFactory (options) {

    let io = require('socket.io-client')

    let { url } = options

    let outBuffer = []
    let inBuffer = {}

    const socket = io(url);

    function startSending(socket){
        while(outBuffer.length>0){
            debug(`Emit Client`)
            socket.emit('Channel',outBuffer.shift())
        }
        setTimeout(()=>{startSending(socket)},500)
    }
    startSending(socket)

    socket.on('Channel',(msg)=>{
        debug(`Client Received ${msg.channel}`)
        debug(msg)
        if(inBuffer[msg.channel]===undefined){
            inBuffer[msg.channel]=[]
        }
        inBuffer[msg.channel].push(msg.value)
    })

    return (name)=> {
        return {
            close: ()=>{socket.close()},
            enqueue: x => {
                debug(`enqueue ${name}`)
                outBuffer.push({ channel: name, value: x })
            },
            length: () => inBuffer[name]!==undefined ? inBuffer[name].length : 0,
            dequeue: () => {
                debug(`dequeue ${name} buffer length:${inBuffer[name].length}`)
                return inBuffer[name].shift()
            }
        }
    }
}

module.exports = ChannelFactory
