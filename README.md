# Real-Value-Channel-SocketIO-Client Library    

An implemententation of channel using socket io client

## How to use

The goal of this functionality is to support the following type of expression
```
model.from([1,2,3]).toChannel('production)
model.fromChannel('production').log()
```

## Install

```
npm install real-value-channel-socketio-client
```


